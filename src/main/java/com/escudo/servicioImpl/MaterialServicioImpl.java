package com.escudo.servicioImpl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.escudo.dominio.Material;
import com.escudo.exception.escudos.MaterialNoExisteException;
import com.escudo.repositorio.IMaterialRepositorio;
import com.escudo.servicio.IMaterialServicio;

@Service
public class MaterialServicioImpl implements IMaterialServicio {

	private static final Log LOG = LogFactory.getLog(MaterialServicioImpl.class);
	
	@Autowired
	private IMaterialRepositorio materialRepositorio;
	
	@Override
	public Material registrar(Material obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public Material modificar(Material obj) {
		Optional<Material> materialOP = materialRepositorio.findById(obj.getId());
		if(materialOP.isPresent()) {
			Material material = materialOP.get();
			material.setNombreProducto(obj.getNombreProducto());
			material.setCantidad(obj.getCantidad());
			material.setLote(obj.getLote());
			material.setUnidad(obj.getUnidad());
			LOG.info(String.format("Material Actualizado %s", material));
			
			return material;
		}
		
		throw new MaterialNoExisteException(obj.getId());
	}

	@Override
	public List<Material> listarTodos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Material listarPorId(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eliminar(Long id) {
		materialRepositorio.deleteById(id);
		LOG.info(String.format("Se elimina el Material id:%s ", id));
		return true;
	}

}
