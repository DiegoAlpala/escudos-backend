package com.escudo.servicioImpl;

import static com.escudo.util.UtilidadesCadena.normalizarDireccionCorreoElectronico;
import static com.escudo.util.UtilidadesCadena.normalizarNombre;

import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.escudo.dominio.SocioTecnico;
import com.escudo.exception.escudos.SocioTecnicoCodigoIdentificadorYaExisteException;
import com.escudo.repositorio.ISocioTecnicoRepositorio;
import com.escudo.servicio.ISocioTecnicoServicio;

@Service
public class SocioTecnicoServicioImpl implements ISocioTecnicoServicio {

	private static final Log LOG = LogFactory.getLog(SocioTecnicoServicioImpl.class);
	
	@Autowired
	private ISocioTecnicoRepositorio socioTecnicoRepositorio;
	
	
	@Override
	public SocioTecnico registrar(SocioTecnico obj) {

		asegurarCodigoIdentificadorUnico(obj.getCodigoIdentificador());

		String nombreNormalizado = normalizarNombre(obj.getNombre());
		String correoNormalizado = normalizarDireccionCorreoElectronico(obj.getCorreo());

		SocioTecnico socio = new SocioTecnico(nombreNormalizado, obj.getCelular(), obj.getTelefono(), correoNormalizado,
				obj.getCodigoIdentificador(), obj.getNombreContacto(), obj.getTelefonoContacto());

		LOG.info(String.format("Socio Técnico a guardar %s", socio));
		return socioTecnicoRepositorio.save(socio);

	}

	@Override
	@Transactional
	public SocioTecnico modificar(SocioTecnico obj) {
		Optional<SocioTecnico> socioOp = socioTecnicoRepositorio.findById(obj.getId());
		if(socioOp.isPresent()) {
			SocioTecnico socio = socioOp.get();
			socio.setActivo(obj.isActivo());
			socio.setCelular(obj.getCelular());
			socio.setCorreo(obj.getCorreo());
			socio.setNombre(obj.getNombre());
			socio.setTelefono(obj.getTelefono());
			socio.setComentario(obj.getComentario());
			socio.setEnlace(obj.getEnlace());
			
			LOG.info(String.format("Socio Técnico Actualizado %s", socio));
			return socio;
		}
		return null;
	}

	@Override
	public List<SocioTecnico> listarTodos() {
		return (List<SocioTecnico>) socioTecnicoRepositorio.findAll();
	}

	@Override
	public SocioTecnico listarPorId(Long id) {
		Optional<SocioTecnico> socio = socioTecnicoRepositorio.findById(id);
		return socio.isPresent() ? socio.get() : null;
	}

	@Override
	public boolean eliminar(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
	
	private void asegurarCodigoIdentificadorUnico(String codigoIdentificador) {
		Optional<SocioTecnico> socioOp = socioTecnicoRepositorio.findByCodigoIdentificador(codigoIdentificador);
		if(socioOp.isPresent()) {
			throw new SocioTecnicoCodigoIdentificadorYaExisteException(codigoIdentificador);
		}
	}

	@Override
	public List<SocioTecnico> listarActivos() {
		return socioTecnicoRepositorio.findByActivoTrue();
	}

}
