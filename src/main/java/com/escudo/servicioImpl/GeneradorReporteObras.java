package com.escudo.servicioImpl;

import static com.escudo.util.UtilidadesPoi.anchoAutomatico;
import static com.escudo.util.UtilidadesPoi.crearCeldaConEstilo;

import java.util.List;

import com.escudo.dominio.ImpermeabilizacionSistema;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Component;

import com.escudo.dominio.dto.ObraConsultaDTO;

@Component
public class GeneradorReporteObras {
    private static final String HOJA_OBRAS_QR = "Obras QR";
    private static final String HOJA_OBRAS_QR_SISTEMAS = "Sistemas Impermeabilizacion";

    private static final int INDICE_COLUMNA_SOCIO = 0;
    private static final int INDICE_COLUMNA_SERIE = 1;
    private static final int INDICE_COLUMNA_TIPO_GARANTIA = 2;
    private static final int INDICE_COLUMNA_ESTADO = 3;
    private static final int INDICE_COLUMNA_ANIOS = 4;
    private static final int INDICE_COLUMNA_REGION = 5;
    private static final int INDICE_COLUMNA_CIUDAD = 6;
    private static final int INDICE_COLUMNA_ASIGNADO = 7;
    private static final int INDICE_COLUMNA_INICIO_GARANTIA = 8;
    private static final int INDICE_COLUMNA_DIMENSION = 9;
    private static final int INDICE_COLUMNA_PROPIETARIO = 10;
    private static final int INDICE_COLUMNA_LATITUD = 11;
    private static final int INDICE_COLUMNA_LONGITUD = 12;

    private static final int INDICE_COLUMNA_SISTEMA_NOMBRE = 2;
    private static final int INDICE_COLUMNA_SISTEMA_TIPO = 3;
    private static final int INDICE_COLUMNA_SISTEMA_TIPO_OBRA = 4;
    private static final int INDICE_COLUMNA_SISTEMA_DIMENSION = 5;

    private void crearCabecera(Sheet sheet, HSSFCellStyle style, final int indiceFila) {

        final Row rowCabecera = sheet.createRow(indiceFila);

        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_SOCIO, style, "SOCIO");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_SERIE, style, "SERIE");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_TIPO_GARANTIA, style, "TIPO GARANTÍA");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_ESTADO, style, "ESTADO ESCUDO");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_ANIOS, style, "AÑOS GARANTÍA");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_REGION, style, "REGIÓN");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_CIUDAD, style, "CIUDAD");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_ASIGNADO, style, "ASIGNADO");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_INICIO_GARANTIA, style, "INICIO GARANTÍA");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_DIMENSION, style, "DIMENSIÓN(m2)");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_PROPIETARIO, style, "PROPIETARIO");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_LONGITUD, style, "LONGITUD");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_LATITUD, style, "LATITUD");
    }

    private void crearCabeceraSistemas(Sheet sheet, HSSFCellStyle style, final int indiceFila) {
        final Row rowCabecera = sheet.createRow(indiceFila);
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_SOCIO, style, "SOCIO");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_SERIE, style, "SERIE");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_SISTEMA_NOMBRE, style, "NOMBRE");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_SISTEMA_TIPO, style, "TIPO SUPERFICIE");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_SISTEMA_TIPO_OBRA, style, "TIPO OBRA");
        crearCeldaConEstilo(rowCabecera, INDICE_COLUMNA_SISTEMA_DIMENSION, style, "DIMENSION");
    }

    private void crearFilaDetalle(Sheet sheet, HSSFCellStyle style, final int indiceFila, ObraConsultaDTO detalle) {

        final Row rowFilaRubro = sheet.createRow(indiceFila);

        crearCeldaConEstilo(rowFilaRubro, INDICE_COLUMNA_SOCIO, style, detalle.getSocio());
        crearCeldaConEstilo(rowFilaRubro, INDICE_COLUMNA_SERIE, style, detalle.getSerie());
        crearCeldaConEstilo(rowFilaRubro, INDICE_COLUMNA_TIPO_GARANTIA, style, detalle.getTipoGarantia().toString());
        crearCeldaConEstilo(rowFilaRubro, INDICE_COLUMNA_ESTADO, style, detalle.getEstado().toString());
        crearCeldaConEstilo(rowFilaRubro, INDICE_COLUMNA_ANIOS, style, String.valueOf(detalle.getAnios()));
        crearCeldaConEstilo(rowFilaRubro, INDICE_COLUMNA_REGION, style, detalle.getRegion() == null ? "" : detalle.getRegion().toString());
        crearCeldaConEstilo(rowFilaRubro, INDICE_COLUMNA_CIUDAD, style, detalle.getCiudad());
        crearCeldaConEstilo(rowFilaRubro, INDICE_COLUMNA_ASIGNADO, style, detalle.getAsignado());
        crearCeldaConEstilo(rowFilaRubro, INDICE_COLUMNA_INICIO_GARANTIA, style, detalle.getFechaInicioGarantia() == null ? "" :
                detalle.getFechaInicioGarantia().toString());
        crearCeldaConEstilo(rowFilaRubro, INDICE_COLUMNA_DIMENSION, style, detalle.getDimension() == null ? "" :
                String.valueOf(detalle.getDimension()));
        crearCeldaConEstilo(rowFilaRubro, INDICE_COLUMNA_PROPIETARIO, style, detalle.getNombrePropietario());
        crearCeldaConEstilo(rowFilaRubro, INDICE_COLUMNA_LONGITUD, style, detalle.getLongitud() == null ? "" : String.valueOf(detalle.getLongitud()));
        crearCeldaConEstilo(rowFilaRubro, INDICE_COLUMNA_LATITUD, style, detalle.getLatitud() == null ? "" : String.valueOf(detalle.getLatitud()));
        style.setDataFormat((short) 14);
    }

    private void crearFilaDetalleSistemas(Sheet sheet, HSSFCellStyle style, final int indiceFila, ImpermeabilizacionSistema sistema, String socio,
                                          String serie) {
        final Row rowFilaSistema = sheet.createRow(indiceFila);
        crearCeldaConEstilo(rowFilaSistema, INDICE_COLUMNA_SOCIO, style, socio);
        crearCeldaConEstilo(rowFilaSistema, INDICE_COLUMNA_SERIE, style, serie);
        crearCeldaConEstilo(rowFilaSistema, INDICE_COLUMNA_SISTEMA_NOMBRE, style, sistema.getNombre());
        crearCeldaConEstilo(rowFilaSistema, INDICE_COLUMNA_SISTEMA_TIPO, style, sistema.getTipo());
        crearCeldaConEstilo(rowFilaSistema, INDICE_COLUMNA_SISTEMA_TIPO_OBRA, style, sistema.getTipoObra());
        crearCeldaConEstilo(rowFilaSistema, INDICE_COLUMNA_SISTEMA_DIMENSION, style, sistema.getDimension() == null ? "" :
                String.valueOf(sistema.getDimension()));
        style.setDataFormat((short) 14);
    }

    private void crearHojaDetalles(Sheet sheet, HSSFCellStyle style, List<ObraConsultaDTO> detalles) {

        int indiceFila = -1;

        crearCabecera(sheet, style, ++indiceFila);

        for (ObraConsultaDTO detalle : detalles) {
            crearFilaDetalle(sheet, style, ++indiceFila, detalle);
        }

        anchoAutomatico(sheet, INDICE_COLUMNA_SOCIO);
        anchoAutomatico(sheet, INDICE_COLUMNA_TIPO_GARANTIA);
        anchoAutomatico(sheet, INDICE_COLUMNA_SERIE);
        anchoAutomatico(sheet, INDICE_COLUMNA_ESTADO);
        anchoAutomatico(sheet, INDICE_COLUMNA_ANIOS);

        anchoAutomatico(sheet, INDICE_COLUMNA_REGION);
        anchoAutomatico(sheet, INDICE_COLUMNA_CIUDAD);
        anchoAutomatico(sheet, INDICE_COLUMNA_ASIGNADO);
        anchoAutomatico(sheet, INDICE_COLUMNA_INICIO_GARANTIA);
        anchoAutomatico(sheet, INDICE_COLUMNA_DIMENSION);
        anchoAutomatico(sheet, INDICE_COLUMNA_PROPIETARIO);
        anchoAutomatico(sheet, INDICE_COLUMNA_LONGITUD);
        anchoAutomatico(sheet, INDICE_COLUMNA_LATITUD);
    }

    private void crearHojaSistemasImpermeabilizacion(Sheet sheet, HSSFCellStyle style, List<ObraConsultaDTO> detalles) {

        int indiceFila = -1;

        crearCabeceraSistemas(sheet, style, ++indiceFila);
        for (ObraConsultaDTO obraDto : detalles) {
            for (ImpermeabilizacionSistema sistema : obraDto.getSistemas()) {
                crearFilaDetalleSistemas(sheet, style, ++indiceFila, sistema, obraDto.getSocio(), obraDto.getSerie());
            }
        }
        anchoAutomatico(sheet, INDICE_COLUMNA_SISTEMA_NOMBRE);
        anchoAutomatico(sheet, INDICE_COLUMNA_SISTEMA_TIPO);
        anchoAutomatico(sheet, INDICE_COLUMNA_SISTEMA_TIPO_OBRA);
        anchoAutomatico(sheet, INDICE_COLUMNA_SISTEMA_DIMENSION);

    }

    public Workbook generarReporte(List<ObraConsultaDTO> detalles) {

        final HSSFWorkbook workbook = new HSSFWorkbook();

        final HSSFCellStyle style = workbook.createCellStyle();

        crearHojaDetalles(workbook.createSheet(HOJA_OBRAS_QR), style, detalles);
        crearHojaSistemasImpermeabilizacion(workbook.createSheet(HOJA_OBRAS_QR_SISTEMAS), style, detalles);

        return workbook;
    }

}
