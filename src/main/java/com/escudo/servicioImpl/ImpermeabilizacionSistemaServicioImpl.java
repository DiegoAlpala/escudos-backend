package com.escudo.servicioImpl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.escudo.dominio.ImpermeabilizacionSistema;
import com.escudo.dominio.Material;
import com.escudo.exception.escudos.SistemaNoExisteException;
import com.escudo.repositorio.IImpermeabilizacionSistemaRepositorio;
import com.escudo.servicio.IImpermeabilizacionSistemaServicio;

@Service
public class ImpermeabilizacionSistemaServicioImpl implements IImpermeabilizacionSistemaServicio {

	private static final Log LOG = LogFactory.getLog(ImpermeabilizacionSistemaServicioImpl.class);
	
	@Autowired
	private IImpermeabilizacionSistemaRepositorio sistemaRepositorio;
	
	@Override
	public ImpermeabilizacionSistema registrar(ImpermeabilizacionSistema obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public ImpermeabilizacionSistema modificar(ImpermeabilizacionSistema obj) {
		
		Optional<ImpermeabilizacionSistema> sistemaOP = sistemaRepositorio.findById(obj.getId());
		if(sistemaOP.isPresent()) {
			ImpermeabilizacionSistema sistema = sistemaOP.get();
			sistema.setNombre(obj.getNombre());
			sistema.setTipo(obj.getTipo());
			sistema.setTipoObra(obj.getTipoObra());
			sistema.setDimension(obj.getDimension());
			LOG.info(String.format("Sistema actualizado %s", sistema));
			return sistema;
		}
		
		throw new SistemaNoExisteException(obj.getId());
		
	}

	@Override
	public List<ImpermeabilizacionSistema> listarTodos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImpermeabilizacionSistema listarPorId(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eliminar(Long id) {
		sistemaRepositorio.deleteById(id);
		LOG.info(String.format("Se elimina el sistema id:%s y todo su detalle de materiales ", id));
		return true;
	}

	@Override
	@Transactional
	public List<Material> agregarMaterial(long id, Material material) {
		Optional<ImpermeabilizacionSistema> sistemaOP = sistemaRepositorio.findById(id);
		if(sistemaOP.isPresent()) {
			ImpermeabilizacionSistema sistema = sistemaOP.get();
			sistema.agregarLineaDetalleMaterial(material);
			LOG.info(String.format("Se agrega al Sistema: %s el Material: %s", sistema.getNombre(), material));
			return sistema.getMateriales();
		}
		
		throw new SistemaNoExisteException(id);
	}

}
