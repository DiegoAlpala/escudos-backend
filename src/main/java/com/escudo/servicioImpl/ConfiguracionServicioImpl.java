package com.escudo.servicioImpl;

import com.escudo.dominio.Configuracion;
import com.escudo.repositorio.ConfiguracionRepositorio;
import com.escudo.servicio.IConfiguracionServicio;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ConfiguracionServicioImpl implements IConfiguracionServicio {
    @Autowired
    private ConfiguracionRepositorio repositorio;

    @Override
    public Configuracion registrar(Configuracion obj) {
        return null;
    }

    @Override
    @Transactional
    public Configuracion modificar(Configuracion obj) {
        Optional<Configuracion> configuracionOP = repositorio.findById(obj.getId());
        if (configuracionOP.isPresent()) {
            Configuracion configuracionActualizar = configuracionOP.get();
            configuracionActualizar.setValorConfiguracion(obj.getValorConfiguracion());
            log.info(String.format("Configuracion %s actualizada", configuracionActualizar.getNombreConfiguracion().getDescripcion()));
            return configuracionActualizar;
        }
        return null;
    }

    @Override
    public List<Configuracion> listarTodos() {
        return null;
    }

    @Override
    public Configuracion listarPorId(Long id) {
        return null;
    }

    @Override
    public boolean eliminar(Long id) {
        return false;
    }

    @Override
    public List<Configuracion> listarConfiguraciones() {
        return (List<Configuracion>) repositorio.findAll();
    }
}
