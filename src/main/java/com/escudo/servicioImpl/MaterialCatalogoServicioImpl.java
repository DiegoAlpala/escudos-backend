package com.escudo.servicioImpl;

import com.escudo.dominio.MaterialCatalogo;
import com.escudo.exception.escudos.MaterialCatalogoYaRegistradoException;
import com.escudo.exception.escudos.MaterialNoExisteException;
import com.escudo.repositorio.IMaterialCatalogoRepositorio;
import com.escudo.servicio.IMaterialCatalogoServicio;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MaterialCatalogoServicioImpl implements IMaterialCatalogoServicio {

	private static final Log LOG = LogFactory.getLog(MaterialCatalogoServicioImpl.class);
	
	@Autowired
	private IMaterialCatalogoRepositorio materialRepositorio;
	
	@Override
	public MaterialCatalogo registrar(MaterialCatalogo obj) {
		MaterialCatalogo material = new MaterialCatalogo(obj.getNombre());
		this.validarUnicoMaterial(obj);
		LOG.info(String.format("Material Regsitrado %s", material));
		return materialRepositorio.save(material);
	}

	@Override
	@Transactional
	public MaterialCatalogo modificar(MaterialCatalogo obj) {
		Optional<MaterialCatalogo> materialOP = materialRepositorio.findById(obj.getId());
		this.validarUnicoMaterial(obj);
		if(materialOP.isPresent()) {
			MaterialCatalogo material = materialOP.get();
			material.setNombre(obj.getNombre());
			material.setActivo(obj.isActivo());
			LOG.info(String.format("Material Actualizado %s", material));
			return material;
		}
		
		throw new MaterialNoExisteException(obj.getId());
	}

	@Override
	public List<MaterialCatalogo> listarTodos() {
		List<MaterialCatalogo> todos = (List<MaterialCatalogo>)	this.materialRepositorio.findAll();
		return todos.stream().sorted(Comparator.comparing(MaterialCatalogo::getNombre)).collect(Collectors.toList());
	}

	@Override
	public MaterialCatalogo listarPorId(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eliminar(Long id) {
		materialRepositorio.deleteById(id);
		LOG.info(String.format("Se elimina el Material id:%s ", id));
		return true;
	}

	@Override
	public List<MaterialCatalogo> listarActivos() {
		return this.materialRepositorio.findByActivoTrueOrderByNombreAsc();
	}

	private void validarUnicoMaterial(MaterialCatalogo material){
		Optional<MaterialCatalogo> materialOP = this.materialRepositorio.findByNombreIgnoreCase(material.getNombre());
		if(materialOP.isPresent()){
			if(materialOP.get().getId() != material.getId())
				throw new MaterialCatalogoYaRegistradoException(material.getNombre());
		}
	}
}
