package com.escudo.servicioImpl.sistema;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.escudo.dominio.sistema.Menu;
import com.escudo.dominio.sistema.Rol;
import com.escudo.dominio.sistema.Roles;
import com.escudo.exception.sistema.RolYaExistenteException;
import com.escudo.repositorio.sistema.IRolRepositorio;
import com.escudo.servicio.sistema.IRolServicio;

@Service
public class RolServicioImpl implements IRolServicio {

	private static final Log LOG = LogFactory.getLog(RolServicioImpl.class);

	@Autowired
	private IRolRepositorio rolRepositorio;

	private void asegurarRolNombreUnico(Roles rolNombre) {

		Optional<Rol> Rol = rolRepositorio.findByNombre(rolNombre);
		if (Rol.isPresent()) {
			throw new RolYaExistenteException(rolNombre.getDescripcion());
		}
	}

	@Override
	public Rol registrar(Rol obj) {
		
		asegurarRolNombreUnico(obj.getNombre());

		LOG.info("Guardando Rol Nuevo: " + obj);

		return rolRepositorio.save(new Rol(obj.getNombre()));
	}

	@Override
	public Rol modificar(Rol obj) {
		LOG.info("Guardando Perfil : " + obj);
		return rolRepositorio.save(obj);
	}

	@Override
	public List<Rol> listarTodos() {
		return (List<Rol>) rolRepositorio.findAll();
	}

	@Override
	public Rol listarPorId(Long id) {
		Optional<Rol> rol = rolRepositorio.findById(id);
		return rol.isPresent() ? rol.get() : null;
	}

	@Override
	public boolean eliminar(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Menu> listarMenuPorRol(Roles nombreRol) {
		Optional<Rol> rolOP = rolRepositorio.findByNombre(nombreRol);
		if(rolOP.isPresent()) {
			return rolOP.get().getDetalleMenu();
		}
		return new ArrayList<>();
	}

}
