package com.escudo.servicioImpl.sistema;

import static com.escudo.util.UtilidadesCadena.normalizarDireccionCorreoElectronico;
import static com.escudo.util.UtilidadesCadena.normalizarNombre;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.escudo.dominio.sistema.Usuario;
import com.escudo.dominio.sistema.UsuarioTipo;
import com.escudo.exception.sistema.UsuarioNoExistenteException;
import com.escudo.exception.sistema.UsuarioYaExistenteException;
import com.escudo.repositorio.sistema.IUsuarioRepositorio;
import com.escudo.servicio.sistema.IUsuarioServicio;

@Service
public class UsuarioServicioImpl implements IUsuarioServicio {

	private static final Log LOG = LogFactory.getLog(UsuarioServicioImpl.class);
	
	@Autowired
	private IUsuarioRepositorio usuarioRepositorio; 
	
	@Autowired
	private GeneradorContrasena generadorContrasena;
	
	@Override
	public Usuario registrar(Usuario obj) {
		String nombreCompletoNormalizado = normalizarNombre(obj.getNombreCompleto());
		String correoNormalizado = normalizarDireccionCorreoElectronico(obj.getCorreo());
		asegurarUsuarioUnico(obj.getNombreUsuario());

		Usuario usuario = new Usuario(obj.getNombreUsuario(), nombreCompletoNormalizado, correoNormalizado,
				generadorContrasena.generar(obj.getContrasena()), obj.getRol(), obj.getTipo());
		LOG.info("Guardando Usuario Nuevo: " + usuario);
		
		return usuarioRepositorio.save(usuario);
	}

	@Override
	@Transactional
	public Usuario modificar(Usuario obj) {
		
		Optional<Usuario> usuarioOP = usuarioRepositorio.findById(obj.getId());
		if(usuarioOP.isPresent()) {
			Usuario usuario = usuarioOP.get();
			
			String correoNormalizado = normalizarDireccionCorreoElectronico(obj.getCorreo());
			String nombreCompletoNormalizado = normalizarNombre(obj.getNombreCompleto());		
			usuario.setCorreo(correoNormalizado);
			usuario.setNombreCompleto(nombreCompletoNormalizado);
			usuario.setRol(obj.getRol());
			usuario.setActivo(obj.isActivo());
			
			LOG.info("Guardando Usuario Modificado : " + usuario);	
			return usuario;
		}
		
		throw new UsuarioNoExistenteException(obj.getId());
		
	}

	@Override
	public List<Usuario> listarTodos() {
		return usuarioRepositorio.findByTipoIn(Arrays.asList(UsuarioTipo.ADMINISTRATIVO,UsuarioTipo.AGENCIA ));
	}

	@Override
	public Usuario listarPorId(Long id) {
		Optional<Usuario> usuario = usuarioRepositorio.findById(id);
		return usuario.isPresent() ? usuario.get() : null;
	}

	@Override
	public boolean eliminar(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
	
	private void asegurarUsuarioUnico(String nombreUsuario) {

		Optional<Usuario> usuario = usuarioRepositorio.findByNombreUsuario(nombreUsuario);
		if (usuario.isPresent()) {
			throw new UsuarioYaExistenteException(nombreUsuario);
		}
	}

	@Override
	@Transactional
	public void cambiarContrasena(long idUsuario, String contrasena) {
		Optional<Usuario> usuarioOp = usuarioRepositorio.findById(idUsuario);
		if(usuarioOp.isPresent()) {
			Usuario usuario = usuarioOp.get();
			
			usuario.setContrasena(generadorContrasena.generar(contrasena));
			LOG.info(String.format("Contraseña cambiada Usuario: %s", usuario.getNombreCompleto()));
		}else {
			throw new UsuarioNoExistenteException(idUsuario);
		}	
	}	

}
