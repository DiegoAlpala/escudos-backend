package com.escudo.servicioImpl.sistema;

import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.escudo.dominio.sistema.Secuencial;
import com.escudo.repositorio.sistema.ISecuencialRepositorio;
import com.escudo.servicio.sistema.ISecuencialServicio;

@Service
public class SecuencialServicioImpl implements ISecuencialServicio {

	private static final Log LOG = LogFactory.getLog(SecuencialServicioImpl.class);

	@Autowired
	private ISecuencialRepositorio secuencialRepositorio;

	public Secuencial ObtenerSecuencialPorDefecto() {

		Optional<Secuencial> secuencialOP = obtenerPrimerSecuencia();

		if (secuencialOP.isPresent()) {
			
			LOG.info(String.format("Secuencial a utilizar %s", secuencialOP));	
			
			Secuencial secuencial = secuencialOP.get();
			secuencial.aumentarSucesion();
			secuencialRepositorio.save(secuencial);

			return secuencial;
		}
		LOG.info(String.format("Secuencial no existente"));
		return null;
	}
	
	private Optional<Secuencial>obtenerPrimerSecuencia() {
		List<Secuencial> secuenciales = (List<Secuencial>) secuencialRepositorio.findAll();		
		return secuenciales.stream().findFirst();
	}

}
