package com.escudo.servicioImpl.sistema;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.escudo.dominio.sistema.Usuario;
import com.escudo.exception.sistema.UsuarioInactivoException;
import com.escudo.repositorio.sistema.IUsuarioRepositorio;

@Service
public class UserServiceImpl implements UserDetailsService {

	@Autowired
	private IUsuarioRepositorio usuarioRepositorio; 
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepositorio.findOneByNombreUsuario(username);
		if(usuario == null)
			throw new UsernameNotFoundException(String.format("Usuario no existe", username));
			
		if(!usuario.isActivo())
			throw new UsuarioInactivoException(username);
		
		List<GrantedAuthority> roles = new ArrayList<>();		 
		roles.add(new SimpleGrantedAuthority(usuario.getRol().getNombre().toString()));
		
		UserDetails ud = new User(usuario.getNombreUsuario(),usuario.getContrasena(), roles);
		
		return ud;
	}

}
