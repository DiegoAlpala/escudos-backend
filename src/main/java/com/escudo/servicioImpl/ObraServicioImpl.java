package com.escudo.servicioImpl;

import static com.escudo.util.UtilidadesCadena.noEsNuloNiBlanco;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.escudo.dominio.*;
import com.escudo.dominio.dto.*;
import com.escudo.exception.JasperReportsException;
import com.escudo.exception.ReporteExeption;
import com.escudo.repositorio.ConfiguracionRepositorio;
import com.escudo.servicio.reporte.IGeneradorJasperReports;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.escudo.exception.escudos.ErrorReporteObrasException;
import com.escudo.exception.escudos.ObraNoExisteException;
import com.escudo.exception.escudos.SocioTecnicoNoExisteException;
import com.escudo.repositorio.IObraRepositorio;
import com.escudo.repositorio.ISocioTecnicoRepositorio;
import com.escudo.servicio.IObraServicio;
import com.escudo.servicio.qr.GeneracionQR;

@Service
public class ObraServicioImpl implements IObraServicio {

    private static final Log LOG = LogFactory.getLog(ObraServicioImpl.class);

    @Autowired
    private IObraRepositorio obraRepositorio;

    @Autowired
    private ISocioTecnicoRepositorio socioRepositorio;

    @Autowired
    private GeneracionQR generarQR;

    @Autowired
    private Environment env;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private GeneradorReporteObras generadorReporte;

    @Autowired
    private ConfiguracionRepositorio configuracionRepositorio;

    @Autowired
    private IGeneradorJasperReports reporteServicio;

    @Override
    public Obra registrar(Obra obj) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    @Transactional
    public Obra modificar(Obra obj) {
        Optional<Obra> obraOp = obraRepositorio.findById(obj.getId());
        if (obraOp.isPresent()) {
            Obra obra = obraOp.get();

            obra.setAnios(obj.getAnios());
            obra.setFechaInicioGarantia(obj.getFechaInicioGarantia());
            obra.setDimension(obj.getDimension());
            obra.setLatitud(obj.getLatitud());
            obra.setLongitud(obj.getLongitud());
            obra.setLote(obj.getLote());
            obra.setNombrePropietario(obj.getNombrePropietario());
            obra.setProducto(obj.getProducto());
            obra.setRazonSocial(obj.getRazonSocial());
            obra.setRegion(obj.getRegion());
            obra.setEstado(ObraEstado.ASIGNADO);
            obra.setUbicacionEscudo(obj.getUbicacionEscudo());
            obra.setCiudad(obj.getCiudad());
            obra.setComentario(obj.getComentario());

            LOG.info(String.format("Obra Modificada %s", obra));
            return obra;
        } else {
            return null;
        }
    }

    @Override
    public List<Obra> listarTodos() {
        return (List<Obra>) obraRepositorio.findAll();
    }

    @Override
    public Obra listarPorId(Long id) {
        Optional<Obra> obra = obraRepositorio.findById(id);
        return obra.isPresent() ? obra.get() : null;
    }

    @Override
    public boolean eliminar(Long id) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Obra listarPorSerie(String serie) {
        Optional<Obra> obra = obraRepositorio.findBySerie(serie);
        if (obra.isPresent()) {
            Configuracion nombre = configuracionRepositorio.findFirstByNombreConfiguracion(NombreConfiguracion.NOMBRE_BROKER).orElse(null);
            if (nombre != null)
                obra.get().setNombreBroker(nombre.getValorConfiguracion());
            Configuracion contacto = configuracionRepositorio.findFirstByNombreConfiguracion(NombreConfiguracion.CONTACTO_BROKER).orElse(null);
            if (contacto != null)
                obra.get().setContactoBroker(contacto.getValorConfiguracion());
        }
        return obra.isPresent() ? obra.get() : null;
    }

    @Override
    public byte[] generarCodigoQR(long id) {

        Optional<Obra> obraOp = obraRepositorio.findById(id);
        if (obraOp.isPresent()) {
            String url = crearURLParaCodigoQR(obraOp.get().getSerie());
            LOG.info(String.format("Se crea codigoQR para la siguiente URL: %s", url));

            File f = new File(String.format("%s.png", obraOp.get().getSerie()));
            try {
                return generarQR.generateQR(f, url, 300, 300);
            } catch (Exception e) {
                LOG.error(e);
            }
        }

        return null;
    }

    private String crearURLParaCodigoQR(String serie) {
        String urlBase = env.getProperty("sistema.urlbase");
        return urlBase.concat("/").concat(serie);
    }

    @Override
    public List<EscudosAsignadosResultadoDTO> asignarEscudos(AsignarEscudoDTO dto) {

        List<EscudosAsignadosResultadoDTO> resultado = new ArrayList<>();

        Optional<SocioTecnico> socioOp = socioRepositorio.findById(dto.getSocio());
        if (socioOp.isPresent()) {
            SocioTecnico socio = socioOp.get();

            LOG.info(String.format("Se asignará %s garantias tipo: %s al socio %s | %s", dto.getCantidad(),
                    dto.getTipoGarantia(), socio.getCodigoIdentificador(), socio.getNombre()));

            for (int i = 0; i < dto.getCantidad(); i++) {
                socio.aumentarSucesion();
                Obra obraAsignar = new Obra(socio.obtenerNumeroSecuencial(), socio,
                        crearEscudoPorDefecto(dto.getTipoGarantia()));

                obraAsignar = obraRepositorio.save(obraAsignar);
                LOG.info(String.format("Se reserva el siguiente secuencial %s para el Socio %s de tipo garantia %s",
                        obraAsignar.getSerie(), socio.getNombre(), dto.getTipoGarantia()));

                resultado.add(new EscudosAsignadosResultadoDTO(obraAsignar.getId(), obraAsignar.getSerie(),
                        obraAsignar.getEscudos().get(0).getTipoGarantia()));
            }

            return resultado;

        } else {
            throw new SocioTecnicoNoExisteException(dto.getSocio());
        }
    }

    private List<EscudoGarantia> crearEscudoPorDefecto(TipoGarantia tipoGarantia) {
        List<EscudoGarantia> escudos = new ArrayList<>();
        escudos.add(new EscudoGarantia(tipoGarantia));
        return escudos;
    }

    @Override
    @Transactional
    public List<ImpermeabilizacionSistema> agregarSistema(long id, ImpermeabilizacionSistema sistema) {
        Optional<Obra> obraOp = obraRepositorio.findById(id);
        if (obraOp.isPresent()) {
            Obra obra = obraOp.get();
            obra.agregarSistema(sistema);

            LOG.info(String.format("Se agrega a la Obra: %s el Sistema: %s", obra.getSerie(), sistema.getNombre()));
            return obra.getSistemas();

        }

        throw new ObraNoExisteException(id);

    }

    @Override
    public List<EscudoGarantia> agregarEscudo(long id, EscudoGarantia escudo) {
        Optional<Obra> obraOp = obraRepositorio.findById(id);
        if (obraOp.isPresent()) {
            Obra obra = obraOp.get();
            obra.agregarEscudo(escudo);
            LOG.info(String.format("Se agrega a la Obra: %s el Escudo tipo: %s", obra.getSerie(), escudo.getTipoGarantia()));
        }

        throw new ObraNoExisteException(id);
    }

    @Override
    public List<ConsultaQR_DTO> consultarQRTodos() {
        List<ConsultaQR_DTO> consulta = new ArrayList<>();
        obraRepositorio.findAll().forEach(x -> {
            consulta.add(new ConsultaQR_DTO(x.getId(), x.getSocio().getNombre(), x.getEstado(), x.getSerie(), x.getEscudos()));
        });
        return consulta;
    }

    @Override
    public Page<ObraConsultaDTO> consultarObrasPaginado(final Pageable pageable, final ConsultaParametrosDTO consulta) {
        try {

//			final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
//			final CriteriaQuery<Obra> query = criteriaBuilder.createQuery(Obra.class);
//
//			final Root<Obra> root = query.from(Obra.class);
//			final List<Predicate> predicadosConsulta = new ArrayList<>();
//			
//			if (consulta.getEstado() != null) {
//				predicadosConsulta.add(criteriaBuilder.equal(root.get("estado"), consulta.getEstado()));
//			}
//
//			if (noEsNuloNiBlanco(consulta.getSerie())) {
//				predicadosConsulta.add(criteriaBuilder.equal(root.get("serie"), consulta.getSerie()));
//			}
//			
//			if (consulta.getIdSocio() != 0) {
//				predicadosConsulta.add(criteriaBuilder.equal(root.get("socio").get("id"), consulta.getIdSocio()));
//			}
//			
//			query.where(predicadosConsulta.toArray(new Predicate[predicadosConsulta.size()]))
//			.orderBy(criteriaBuilder.desc(root.get("modificadoFecha")));
//			
//			final TypedQuery<Obra> statement = this.entityManager.createQuery(query);
//
//			List<Obra> obrasResult = statement.getResultList();

            List<ObraConsultaDTO> resultado = consultarObrasPorParametros(consulta);

//			if(consulta.getTipoGarantia() != null) {
//				for (Obra r : obrasResult) {
//					if(!r.getEscudos().isEmpty()) {
//						resultado.add(new ObraConsultaDTO(r.getSocio().getNombre(), r.getSerie(), r.getEstado(), r.getEscudos().get(0)
//						.getTipoGarantia(), r.getSocio().isActivo()));
//					}
//				}					
//				
//			}else {
//				for (Obra r : obrasResult) {
//					resultado.add(new ObraConsultaDTO(r.getSocio().getNombre(), r.getSerie(), r.getEstado(), r.getEscudos().get(0).getTipoGarantia()
//					, r.getSocio().isActivo()));
//				};
//			}
//			

            final int sizeTotal = resultado.size();

            final int start = (int) pageable.getOffset();
            final int end = (start + pageable.getPageSize()) > resultado.size() ? resultado.size()
                    : (start + pageable.getPageSize());

            resultado = resultado.subList(start, end);

            final Page<ObraConsultaDTO> pageResut = new PageImpl<>(resultado, pageable, sizeTotal);

            return pageResut;

        } catch (final Exception ex) {
            final Page<ObraConsultaDTO> pageResult = new PageImpl<>(new ArrayList<>(), pageable, 0);
            return pageResult;
        }
    }

    private List<ObraConsultaDTO> consultarObrasPorParametros(ConsultaParametrosDTO consulta) {
        try {
            final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
            final CriteriaQuery<Obra> query = criteriaBuilder.createQuery(Obra.class);

            final Root<Obra> root = query.from(Obra.class);
            final List<Predicate> predicadosConsulta = new ArrayList<>();

            if (consulta.getEstado() != null) {
                predicadosConsulta.add(criteriaBuilder.equal(root.get("estado"), consulta.getEstado()));
            }

            if (noEsNuloNiBlanco(consulta.getSerie())) {
                predicadosConsulta.add(criteriaBuilder.equal(root.get("serie"), consulta.getSerie()));
            }

            if (consulta.getIdSocio() != 0) {
                predicadosConsulta.add(criteriaBuilder.equal(root.get("socio").get("id"), consulta.getIdSocio()));
            }

            query.where(predicadosConsulta.toArray(new Predicate[predicadosConsulta.size()]))
                    .orderBy(criteriaBuilder.desc(root.get("modificadoFecha")));

            final TypedQuery<Obra> statement = this.entityManager.createQuery(query);

            List<Obra> obrasResult = statement.getResultList();

            List<ObraConsultaDTO> resultado = new ArrayList<>();

            if (consulta.getTipoGarantia() != null) {
                obrasResult.stream().filter(x -> x.getEscudos().stream().anyMatch(es -> es.getTipoGarantia().equals(consulta.getTipoGarantia()))).forEach(r -> {
                    resultado.add(new ObraConsultaDTO(r));
                });

            } else {
                for (Obra r : obrasResult) {
                    resultado.add(new ObraConsultaDTO(r));
                }
            }

            return resultado;

        } catch (final Exception ex) {
            LOG.error(String.format("Error al generar la consulta: %s", ex));
            return new ArrayList<>();
        }
    }

    @Override
    public byte[] consultarObrasTotalExcel(ConsultaParametrosDTO consulta) {

        try {
            List<ObraConsultaDTO> resultado = consultarObrasPorParametros(consulta);
            final Workbook workbook = generadorReporte.generarReporte(resultado);
            ByteArrayOutputStream ms = new ByteArrayOutputStream();
            workbook.write(ms);
            return ms.toByteArray();

        } catch (IOException e) {
            LOG.error(String.format("Error al momento de genrear el reporte de Obras Qr %s", e.getCause()));
            throw new ErrorReporteObrasException();
        }

    }

    @Transactional(readOnly = true)
    @Override
    public byte[] generateReporteEscudo(long id) {
        Optional<Obra> obraOptional = obraRepositorio.findById(id);
        if (obraOptional.isPresent()) {
            try {
                ReporteEscudoQrDTO dto = this.crearDtoReporte(obraOptional.get());
                return reporteServicio.generarReporte("obra_escudo", Collections.singleton(dto), new HashMap<>());

            } catch (JasperReportsException e) {
                LOG.error(String.format("Error Product Reporte: %s", e.getMessage()));
                throw new ReporteExeption("Product");
            }
        }
        return null;
    }

    private ReporteEscudoQrDTO crearDtoReporte(Obra obra) {
        byte[] qrBytes = generarCodigoQR(obra.getId());
        if (qrBytes == null)
            throw new ErrorReporteObrasException();

        return new ReporteEscudoQrDTO(
                obra.getSocio().getNombre(),
                this.generarQRArchivo(obra),
                this.determinarImagenEscudo(obra));
    }

    private String determinarImagenEscudo(Obra obra) {
        EscudoGarantia escudo = obra.getEscudos().stream().findFirst().orElse(null);
        if (escudo != null) {
            try {
                switch (escudo.getTipoGarantia()) {
                    case ORO:
                        final File fileOro = new ClassPathResource("/img/Escudo_IMPTEK_ORO.jpg").getFile();
                        return fileOro.getPath();
                    case PLATA:
                        final File filePlata = new ClassPathResource("/img/Escudo_IMPTEK_PLATA.jpg").getFile();
                        return filePlata.getPath();
                    case BRONCE:
                        final File fileBronce = new ClassPathResource("/img/Escudo_IMPTEK_BRONCE.jpg").getFile();
                        return fileBronce.getPath();
                    case PLATINO:
                        final File filePlatino = new ClassPathResource("/img/Escudo_IMPTEK_PLATINO.jpg").getFile();
                        return filePlatino.getPath();
                    default:
                        throw new ReporteExeption("Product");
                }
            } catch (IOException e) {
                e.printStackTrace();
                throw new ReporteExeption("Product");
            }
        } else return "";
    }

    private String imagenABase64(byte[] bytesImagen) {
        String base64File = Base64.getEncoder().encodeToString(bytesImagen);
        return String.format("data:image/png;base64,%s", base64File);
    }

    private String generarQRArchivo(Obra obra) {
        String url = crearURLParaCodigoQR(obra.getSerie());
        LOG.info(String.format("Se crea codigoQR para la siguiente URL: %s", url));

        File f = new File(String.format("%s.png", obra.getSerie()));
        try {
            generarQR.generateQR(f, url, 300, 300);
            return f.getPath();
        } catch (Exception e) {
            LOG.error(e);
        }
        return null;
    }


}