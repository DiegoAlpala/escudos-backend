package com.escudo.exception.sistema;

@SuppressWarnings("serial")
public class UsuarioInactivoException  extends SistemaException{

	private String nombre;
	
	public UsuarioInactivoException(String codigo) {
		this.nombre = codigo;
	}
	
	@Override
	public String getMessage() {		
		return String.format("Usuario %s inactivo.", nombre);
	}

}
