package com.escudo.exception.sistema;

@SuppressWarnings("serial")
public class RolYaExistenteException  extends SistemaException{

	private String codigo;
	
	public RolYaExistenteException(String codigo) {
		this.codigo = codigo;
	}
	
	@Override
	public String getMessage() {		
		return String.format("Rol %s ya existe", codigo);
	}

}
