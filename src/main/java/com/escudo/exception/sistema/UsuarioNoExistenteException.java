package com.escudo.exception.sistema;

@SuppressWarnings("serial")
public class UsuarioNoExistenteException  extends SistemaException{

	private long id;
	
	public UsuarioNoExistenteException(long id) {
		this.id = id;
	}
	
	@Override
	public String getMessage() {		
		return String.format("Usuario id:%s no existe", id);
	}

}
