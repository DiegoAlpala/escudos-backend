package com.escudo.exception.sistema;

@SuppressWarnings("serial")
public class SecuencialNoExisteException  extends SistemaException{
	
	@Override
	public String getMessage() {		
		return String.format("Secuencial no existe");
	}

}
