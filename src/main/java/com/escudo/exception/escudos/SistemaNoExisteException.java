package com.escudo.exception.escudos;

@SuppressWarnings("serial")
public class SistemaNoExisteException  extends EscudosException{

	private Long id;
	
	public SistemaNoExisteException(Long codigo) {
		this.id = codigo;
	}
	
	@Override
	public String getMessage() {		
		return String.format("Sistema id: %s no existe.", id);
	}

}
