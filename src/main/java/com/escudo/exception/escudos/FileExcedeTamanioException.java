package com.escudo.exception.escudos;

@SuppressWarnings("serial")
public class FileExcedeTamanioException  extends EscudosException{
	
	@Override
	public String getMessage() {		
		return String.format("File excede el tamaño");
	}

}
