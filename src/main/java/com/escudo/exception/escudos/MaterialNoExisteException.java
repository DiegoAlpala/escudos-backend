package com.escudo.exception.escudos;

@SuppressWarnings("serial")
public class MaterialNoExisteException  extends EscudosException{

	private Long id;
	
	public MaterialNoExisteException(Long codigo) {
		this.id = codigo;
	}
	
	@Override
	public String getMessage() {		
		return String.format("Material id: %s no existe.", id);
	}

}
