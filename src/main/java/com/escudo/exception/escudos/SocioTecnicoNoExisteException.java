package com.escudo.exception.escudos;

@SuppressWarnings("serial")
public class SocioTecnicoNoExisteException  extends EscudosException{

	private Long id;
	
	public SocioTecnicoNoExisteException(Long codigo) {
		this.id = codigo;
	}
	
	@Override
	public String getMessage() {		
		return String.format("Socio Técnico id: %s no existe.", id);
	}

}
