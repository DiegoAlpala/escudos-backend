package com.escudo.exception.escudos;

@SuppressWarnings("serial")
public abstract class EscudosException extends RuntimeException {

	@Override
	public abstract String getMessage();
}
