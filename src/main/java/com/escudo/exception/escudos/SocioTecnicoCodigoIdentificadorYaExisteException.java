package com.escudo.exception.escudos;

@SuppressWarnings("serial")
public class SocioTecnicoCodigoIdentificadorYaExisteException  extends EscudosException{

	private String codigoIdentificador;
	
	public SocioTecnicoCodigoIdentificadorYaExisteException(String codigo) {
		this.codigoIdentificador = codigo;
	}
	
	@Override
	public String getMessage() {		
		return String.format("EL Código Identificador %s ya existe para otro Socio Técnico.", codigoIdentificador);
	}

}
