package com.escudo.exception.escudos;

@SuppressWarnings("serial")
public class ObraNoExisteException  extends EscudosException{

	private Long id;
	
	public ObraNoExisteException(Long codigo) {
		this.id = codigo;
	}
	
	@Override
	public String getMessage() {		
		return String.format("Obra id: %s no existe.", id);
	}

}
