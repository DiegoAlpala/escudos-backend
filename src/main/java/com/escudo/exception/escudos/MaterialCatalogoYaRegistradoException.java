package com.escudo.exception.escudos;

@SuppressWarnings("serial")
public class MaterialCatalogoYaRegistradoException extends EscudosException{

	private String nombre;

	public MaterialCatalogoYaRegistradoException(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String getMessage() {		
		return String.format("Material %s ya se encuentra registrado.", nombre);
	}

}
