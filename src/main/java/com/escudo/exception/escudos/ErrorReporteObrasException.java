package com.escudo.exception.escudos;

@SuppressWarnings("serial")
public class ErrorReporteObrasException  extends EscudosException{
	
	@Override
	public String getMessage() {		
		return "Error al momento de genrerar el reporte";
	}

}
