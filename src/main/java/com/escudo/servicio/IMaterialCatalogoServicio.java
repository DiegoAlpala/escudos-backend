package com.escudo.servicio;

import com.escudo.dominio.MaterialCatalogo;
import com.escudo.servicio.base.ICRUD;

import java.util.List;

public interface IMaterialCatalogoServicio extends ICRUD<MaterialCatalogo, Long> {
	List<MaterialCatalogo> listarActivos();
	
}
