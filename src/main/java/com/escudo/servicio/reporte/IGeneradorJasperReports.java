package com.escudo.servicio.reporte;

import com.escudo.exception.JasperReportsException;

import java.util.Collection;
import java.util.Map;

public interface IGeneradorJasperReports {

	 byte[] generarReporte(final String reporteNombre, final Collection<?> objetos,
			Map<String, Object> parametrosEspecificos) throws JasperReportsException;
}
