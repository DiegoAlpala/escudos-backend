package com.escudo.servicio.sistema;

import java.util.List;

import com.escudo.dominio.sistema.Menu;
import com.escudo.dominio.sistema.Rol;
import com.escudo.dominio.sistema.Roles;
import com.escudo.servicio.base.ICRUD;

public interface IRolServicio extends ICRUD<Rol, Long> {
	
	List<Menu> listarMenuPorRol(Roles nombreRol);
}
