package com.escudo.servicio.sistema;

import com.escudo.dominio.sistema.Usuario;
import com.escudo.servicio.base.ICRUD;

public interface IUsuarioServicio extends ICRUD<Usuario, Long> {

	void cambiarContrasena(long idUsuario, String contrasena);
}
