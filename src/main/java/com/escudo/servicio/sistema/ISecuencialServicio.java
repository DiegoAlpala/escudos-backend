package com.escudo.servicio.sistema;

import com.escudo.dominio.sistema.Secuencial;

public interface ISecuencialServicio {
	
	Secuencial ObtenerSecuencialPorDefecto();
}
