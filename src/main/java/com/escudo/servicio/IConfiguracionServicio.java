package com.escudo.servicio;

import com.escudo.dominio.Configuracion;
import com.escudo.servicio.base.ICRUD;

import java.util.List;

public interface IConfiguracionServicio extends ICRUD<Configuracion, Long> {

    List<Configuracion> listarConfiguraciones();
}
