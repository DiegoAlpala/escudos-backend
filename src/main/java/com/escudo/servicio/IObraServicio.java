package com.escudo.servicio;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.escudo.dominio.EscudoGarantia;
import com.escudo.dominio.ImpermeabilizacionSistema;
import com.escudo.dominio.Obra;
import com.escudo.dominio.dto.AsignarEscudoDTO;
import com.escudo.dominio.dto.ConsultaParametrosDTO;
import com.escudo.dominio.dto.ConsultaQR_DTO;
import com.escudo.dominio.dto.EscudosAsignadosResultadoDTO;
import com.escudo.dominio.dto.ObraConsultaDTO;
import com.escudo.servicio.base.ICRUD;

public interface IObraServicio extends ICRUD<Obra, Long> {

	Obra listarPorSerie(String serie);
	
	byte[] generarCodigoQR(long id);
	
	List<EscudosAsignadosResultadoDTO> asignarEscudos(AsignarEscudoDTO dto);
	
	List<ImpermeabilizacionSistema> agregarSistema(long id, ImpermeabilizacionSistema sistema);
	
	List<EscudoGarantia> agregarEscudo(long id, EscudoGarantia escudo);
	
	List<ConsultaQR_DTO> consultarQRTodos();
	
	Page<ObraConsultaDTO> consultarObrasPaginado(Pageable pageabe,ConsultaParametrosDTO consulta);
	
	byte[] consultarObrasTotalExcel(ConsultaParametrosDTO consulta);

	byte[] generateReporteEscudo(long id);
}
