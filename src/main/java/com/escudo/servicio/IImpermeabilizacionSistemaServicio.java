package com.escudo.servicio;

import java.util.List;

import com.escudo.dominio.ImpermeabilizacionSistema;
import com.escudo.dominio.Material;
import com.escudo.servicio.base.ICRUD;

public interface IImpermeabilizacionSistemaServicio extends ICRUD<ImpermeabilizacionSistema, Long> {
	
	List<Material> agregarMaterial(long id, Material material);
	
}
