package com.escudo.servicio;

import java.util.List;

import com.escudo.dominio.SocioTecnico;
import com.escudo.servicio.base.ICRUD;

public interface ISocioTecnicoServicio extends ICRUD<SocioTecnico, Long> {

	List<SocioTecnico> listarActivos();
}
