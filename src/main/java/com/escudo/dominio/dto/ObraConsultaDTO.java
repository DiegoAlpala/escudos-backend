package com.escudo.dominio.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.escudo.dominio.*;
import com.escudo.util.LocalDateDeserialize;
import com.escudo.util.LocalDateSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class ObraConsultaDTO implements Serializable {

	private String socio;
	
	private String serie;
	
	private ObraEstado estado;
	
	private TipoGarantia tipoGarantia;
	
	private boolean estadoSocio;

	private int anios = 0;
	private Region region;
	private LocalDate fechaInicioGarantia;
	private BigDecimal longitud = BigDecimal.ZERO;
	private BigDecimal latitud = BigDecimal.ZERO;
	private BigDecimal dimension = BigDecimal.ZERO;
	private String nombrePropietario;
	private String razonSocial;
	private String comentario;
	private String ciudad;
	private List<ImpermeabilizacionSistema> sistemas = new ArrayList<>();

	public ObraConsultaDTO() {	}

	public ObraConsultaDTO(Obra obra) {
		this.socio = obra.getSocio().getNombre();
		this.serie = obra.getSerie();
		this.estado = obra.getEstado();
		this.tipoGarantia = obra.getEscudos().isEmpty() ? null: obra.getEscudos().get(0).getTipoGarantia();
		this.estadoSocio = obra.getSocio().isActivo();
		this.anios = obra.getAnios();
		this.region = obra.getRegion();
		this.fechaInicioGarantia = obra.getFechaInicioGarantia();
		this.longitud = obra.getLongitud();
		this.latitud = obra.getLatitud();
		this.dimension = obra.getDimension();
		this.nombrePropietario = obra.getNombrePropietario();
		this.razonSocial = obra.getRazonSocial();
		this.comentario = obra.getComentario();
		this.ciudad = obra.getCiudad();
		this.sistemas = obra.getSistemas();
	}

	public String getAsignado(){
		return getEstado().equals(ObraEstado.ASIGNADO)?"SI":"NO";
	}
}
