package com.escudo.dominio.dto;

import java.io.Serializable;

import com.escudo.dominio.TipoGarantia;

@SuppressWarnings("serial")
public class EscudosAsignadosResultadoDTO implements Serializable {

	private long id;
	
	private String serie;
	
	private TipoGarantia tipoGarantia;

	public EscudosAsignadosResultadoDTO() {	}

	public EscudosAsignadosResultadoDTO(long id, String serie, TipoGarantia tipoGarantia) {
		this.id = id;
		this.serie = serie;
		this.tipoGarantia = tipoGarantia;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public TipoGarantia getTipoGarantia() {
		return tipoGarantia;
	}

	public void setTipoGarantia(TipoGarantia tipoGarantia) {
		this.tipoGarantia = tipoGarantia;
	}
	
}
