package com.escudo.dominio.dto;

import java.io.Serializable;

import com.escudo.dominio.TipoGarantia;

@SuppressWarnings("serial")
public class AsignarEscudoDTO implements Serializable{

	private long socio;
	
	private TipoGarantia tipoGarantia;
	
	private int cantidad;

	public long getSocio() {
		return socio;
	}

	public TipoGarantia getTipoGarantia() {
		return tipoGarantia;
	}

	public int getCantidad() {
		return cantidad;
	}
}
