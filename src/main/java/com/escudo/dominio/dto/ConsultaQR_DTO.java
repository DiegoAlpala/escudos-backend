package com.escudo.dominio.dto;

import java.io.Serializable;
import java.util.List;

import com.escudo.dominio.EscudoGarantia;
import com.escudo.dominio.ObraEstado;
import com.escudo.dominio.TipoGarantia;

@SuppressWarnings("serial")
public class ConsultaQR_DTO implements Serializable {

	private long id;
	
	private String socio;
	
	private ObraEstado estado;
	
	private TipoGarantia tipoGarantia;
	
	private String serie;
	
	public ConsultaQR_DTO() {	}

	public ConsultaQR_DTO(long id, String socio, ObraEstado estado, String serie, List<EscudoGarantia> escudos) {
		super();
		this.id = id;
		this.socio = socio;
		this.estado = estado;
		this.serie = serie;
		this.determinarGarantia(escudos);
	}
	
	private void determinarGarantia(List<EscudoGarantia> escudos) {
		if(!escudos.isEmpty())
			this.tipoGarantia = escudos.get(0).getTipoGarantia();
	}

	public long getId() {
		return id;
	}

	public String getSocio() {
		return socio;
	}

	public ObraEstado getEstado() {
		return estado;
	}

	public TipoGarantia getTipoGarantia() {
		return tipoGarantia;
	}

	public String getSerie() {
		return serie;
	}
	
	
	
}
