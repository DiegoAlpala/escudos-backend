package com.escudo.dominio.dto;

import java.io.Serializable;

import com.escudo.dominio.ObraEstado;
import com.escudo.dominio.TipoGarantia;

@SuppressWarnings("serial")
public class ConsultaParametrosDTO implements Serializable {

	private long idSocio;
	
	private ObraEstado estado;
	
	private TipoGarantia tipoGarantia;
	
	private String serie;

	public long getIdSocio() {
		return idSocio;
	}

	public ObraEstado getEstado() {
		return estado;
	}

	public TipoGarantia getTipoGarantia() {
		return tipoGarantia;
	}

	public String getSerie() {
		return serie;
	}
	
	
}
