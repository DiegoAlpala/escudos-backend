package com.escudo.dominio.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ReporteEscudoQrDTO implements Serializable {
    private String socioNombre;
    private String qrBase64;
    private String rutaImagenEscudo;

    public ReporteEscudoQrDTO(String socioNombre, String qrBase64, String rutaImagenEscudo) {
        this.socioNombre = socioNombre;
        this.qrBase64 = qrBase64;
        this.rutaImagenEscudo = rutaImagenEscudo;
    }
}
