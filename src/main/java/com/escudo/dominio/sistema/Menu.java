package com.escudo.dominio.sistema;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.escudo.dominio.base.EntidadBase;

@SuppressWarnings("serial")
@Entity
public class Menu extends EntidadBase {
	
	@Column(name = "icono", nullable = false, length = 64)
	private String icono;
	
	@NotNull
	@Size( max= 50 , message="nombre debe tener máximo 50 caractéres")
	private String nombre;	

	@NotNull
	@Size( max= 1024 , message="url debe tener máximo 1024 caractéres")
	private String url;
	
	private int orden;
	
	@OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "menu_id", nullable = false)
	private List<SubMenu> subMenus;

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	public List<SubMenu> getSubMenus() {
		return subMenus;
	}

	public void setSubMenus(List<SubMenu> subMenus) {
		this.subMenus = subMenus;
	}
	
}
