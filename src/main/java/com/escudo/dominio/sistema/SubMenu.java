package com.escudo.dominio.sistema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.escudo.dominio.base.EntidadBase;

@SuppressWarnings("serial")
@Entity
public class SubMenu extends EntidadBase {
	
	@Column(name = "icono", nullable = false, length = 64)
	private String icono;
	
	@NotNull
	@Size( max= 50 , message="nombre debe tener máximo 50 caractéres")
	private String nombre;	

	@NotNull
	@Size( max= 1024 , message="url debe tener máximo 1024 caractéres")
	private String url;
	
	private int orden;

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}
	
}
