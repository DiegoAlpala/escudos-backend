package com.escudo.dominio.sistema;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.escudo.dominio.base.EntidadBase;

@SuppressWarnings("serial")
@Entity
public class Rol extends EntidadBase {

	@NotNull
	@Enumerated(EnumType.STRING)
	private Roles  nombre;
	
	private String  descripcion;
	
	private boolean activo;
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true)
	@JoinColumn(name = "rol_id", nullable = false)
	private List<Menu> detalleMenu;
	
	public Rol() {	}

	public Rol(Roles nombre) {
		this.nombre = nombre;
	}

	public Roles getNombre() {
		return nombre;
	}

	public void setNombre(Roles nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public List<Menu> getDetalleMenu() {
		return detalleMenu;
	}

	public void setDetalleMenu(List<Menu> detalleMenu) {
		this.detalleMenu = detalleMenu;
	}
	
}
