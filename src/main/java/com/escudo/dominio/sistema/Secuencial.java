package com.escudo.dominio.sistema;

import static com.escudo.util.UtilidadesCadena.completarCerosIzquierda;

import javax.persistence.Entity;

import com.escudo.dominio.base.EntidadBase;

@SuppressWarnings("serial")
@Entity
public class Secuencial extends EntidadBase {

	private static final int LONGITUD_NUMERO = 10;
	
	private int sucesion;
	
	private String numeroSecuencial;
	
	public int getSucesion() {
		return sucesion;
	}

	public void setSucesion(int sucesion) {
		this.sucesion = sucesion;
	}

	public void setNumeroSecuencial(String numeroSecuencial) {
		this.numeroSecuencial = numeroSecuencial;
	}

	public String getNumeroSecuencial() {
		numeroSecuencial = completarCerosIzquierda(this.sucesion, LONGITUD_NUMERO); 
		return numeroSecuencial;
	}
	
	public int aumentarSucesion() {
			return sucesion++;
	}
	
}