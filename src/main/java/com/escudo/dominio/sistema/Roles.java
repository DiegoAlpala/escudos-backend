package com.escudo.dominio.sistema;

public enum Roles {

	SUPERADMIN("SUPERADMIN"),
	ADMINISTRADOR("ADMINISTRADOR"),
	AGENCIA("AGENCIA"),
	SOCIO_TECNICO("SOCIO TÉCNICO");
	
	private String descripcion;
	
	private Roles(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}	
	
}
