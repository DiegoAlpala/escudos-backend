package com.escudo.dominio.sistema;

public enum UsuarioTipo {

	ADMINISTRATIVO("ADMINISTRATIVO"),
	SOCIO("SOCIO"),
	AGENCIA("AGENCIA");
	
	private String descripcion;
	
	private UsuarioTipo(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}	
	
}
