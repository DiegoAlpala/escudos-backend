package com.escudo.dominio.sistema;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.escudo.dominio.base.EntidadBase;

@SuppressWarnings("serial")
@Entity
public class Usuario extends EntidadBase {

	@NotNull
	@NotBlank
	private String nombreUsuario;

	@NotNull
	@NotBlank
	private String nombreCompleto;

	@NotBlank
	private String correo;

	@NotNull
	@NotBlank
	private String contrasena;

	private boolean activo;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rol_id", referencedColumnName = "id", insertable = true, updatable = true)
	private Rol rol;
	
	@Enumerated(EnumType.STRING)
	private UsuarioTipo tipo;
	
	public Usuario() {	}

	public Usuario(String nombreUsuario, String nombreCompleto,
			String correo, String contrasena, Rol rol, UsuarioTipo tipo) {
		super();
		this.nombreUsuario = nombreUsuario;
		this.nombreCompleto = nombreCompleto;
		this.correo = correo;
		this.contrasena = contrasena;
		this.rol = rol;
		this.activo = Boolean.TRUE;
		this.tipo = tipo;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}
	
	public UsuarioTipo getTipo() {
		return tipo;
	}

	@Override
	public String toString() {
		return "Usuario [nombreUsuario=" + nombreUsuario + ", nombreCompleto=" + nombreCompleto + ", correo=" + correo
				+ ", contrasena=" + contrasena + ", activo=" + activo + ", rol=" + rol + ", tipo=" + tipo + "]";
	}
	
}
