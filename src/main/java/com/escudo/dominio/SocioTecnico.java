package com.escudo.dominio;

import static com.escudo.util.UtilidadesCadena.completarCerosIzquierda;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import com.escudo.dominio.base.EntidadBase;

@SuppressWarnings("serial")
@Entity
public class SocioTecnico extends EntidadBase {

	private static final int LONGITUD_NUMERO = 10;
	
	@NotNull
	private String nombre;
	
	private String celular;
	
	private String telefono;
	
	private String correo;
	
	private boolean activo;
	
	@NotNull
	private String codigoIdentificador;
	
	private int secuencial = 0;
	
	private String numeroSecuencial;
	
	private String nombreContacto;
	
	private String telefonoContacto;

	@Column(columnDefinition = "varchar(max)")
	private String comentario;

	@Column(columnDefinition = "varchar(max)")
	private String enlace;

	public SocioTecnico() {	}

	public SocioTecnico(String nombre, String celular, String telefono, String correo,
			String codigoIdentificador, String nombreContacto, String telefonoContacto) {
		super();
		this.nombre = nombre;
		this.celular = celular;
		this.telefono = telefono;
		this.correo = correo;
		this.codigoIdentificador = codigoIdentificador;
		this.activo = Boolean.TRUE;
		this.nombreContacto = nombreContacto;
		this.telefonoContacto = telefonoContacto;
		this.secuencial = 0;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getCodigoIdentificador() {
		return codigoIdentificador;
	}

	public void setCodigoIdentificador(String codigoIdentificador) {
		this.codigoIdentificador = codigoIdentificador;
	}

	public int getSecuencial() {
		return secuencial;
	}

	public void setSecuencial(int secuencial) {
		this.secuencial = secuencial;
	}

	public String getNombreContacto() {
		return nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	public String getTelefonoContacto() {
		return telefonoContacto;
	}

	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}
	
	public String obtenerNumeroSecuencial() {
		numeroSecuencial = getCodigoIdentificador().concat(completarCerosIzquierda(this.secuencial, LONGITUD_NUMERO)); 
		return numeroSecuencial;
	}
	
	public int aumentarSucesion() {
			return secuencial++;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getEnlace() {
		return enlace;
	}

	public void setEnlace(String enlace) {
		this.enlace = enlace;
	}

	@Override
	public String toString() {
		return "SocioTecnico [nombre=" + nombre + ", celular=" + celular + ", telefono=" + telefono + ", correo="
				+ correo + ", activo=" + activo + ", codigoIdentificador=" + codigoIdentificador + ", nombreContacto="
				+ nombreContacto + ", telefonoContacto=" + telefonoContacto + "]";
	}
	
}
