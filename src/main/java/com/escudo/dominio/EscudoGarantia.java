package com.escudo.dominio;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

import com.escudo.dominio.base.EntidadBase;

@SuppressWarnings("serial")
@Entity
public class EscudoGarantia extends EntidadBase {
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private TipoGarantia tipoGarantia;
	
	@Enumerated(EnumType.STRING)
	private EscudoEstado estado;
	
	public EscudoGarantia() {	}
	
	public EscudoGarantia(TipoGarantia tipoGarantia) {
		this.tipoGarantia = tipoGarantia;
		this.estado = EscudoEstado.PENDIENTE;
	}
	public TipoGarantia getTipoGarantia() {
		return tipoGarantia;
	}

	public void setTipoGarantia(TipoGarantia tipoGarantia) {
		this.tipoGarantia = tipoGarantia;
	}

	public EscudoEstado getEstado() {
		return estado;
	}

	public void setEstado(EscudoEstado estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "EscudoGarantia [tipoGarantia=" + tipoGarantia + ", estado=" + estado + "]";
	}
}
