package com.escudo.dominio;

public enum NombreConfiguracion {
    NOMBRE_BROKER("Nombre Broker"),
    CONTACTO_BROKER("Contacto Broker");

    private String descripcion;

    private NombreConfiguracion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}

