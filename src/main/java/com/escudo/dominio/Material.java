package com.escudo.dominio;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.escudo.dominio.base.EntidadBase;

@SuppressWarnings("serial")
@Entity
public class Material extends EntidadBase {

	private String nombreProducto;
	
	private String lote;
	
	private BigDecimal cantidad;

	private String unidad;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "material_catalogo_id", referencedColumnName = "id")
	private MaterialCatalogo materialCatalogo;

	public Material() {	}

	public Material(MaterialCatalogo materialCatalogo, String lote, BigDecimal cantidad, String unidad) {
		super();
		this.nombreProducto = materialCatalogo.getNombre();
		this.lote = lote;
		this.cantidad = cantidad;
		this.unidad = unidad;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public MaterialCatalogo getMaterialCatalogo() {
		return materialCatalogo;
	}

	public void setMaterialCatalogo(MaterialCatalogo materialCatalogo) {
		this.materialCatalogo = materialCatalogo;
	}

	@Override
	public String toString() {
		return "Material [nombreProducto=" + nombreProducto + ", lote=" + lote + ", cantidad=" + cantidad + ", unidad="
				+ unidad + "]";
	}
	
}
