package com.escudo.dominio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.escudo.dominio.base.EntidadBase;

@SuppressWarnings("serial")
@Entity
public class ImpermeabilizacionSistema extends EntidadBase {

	private String nombre;
	
	private String tipo;
	
	private String tipoObra;
	
	private BigDecimal dimension = BigDecimal.ZERO;
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true)
	@JoinColumn(name = "sistema_id", nullable = false)
	private List<Material> materiales = new ArrayList<>();
	
	@Column(columnDefinition = "varchar(MAX)")
	private String descripcion;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipoObra() {
		return tipoObra;
	}

	public void setTipoObra(String tipoObra) {
		this.tipoObra = tipoObra;
	}

	public BigDecimal getDimension() {
		return dimension;
	}

	public void setDimension(BigDecimal dimension) {
		this.dimension = dimension;
	}

	public List<Material> getMateriales() {
		return materiales;
	}

	public void setMateriales(List<Material> materiales) {
		this.materiales = materiales;
	}
	
	public void agregarLineaDetalleMaterial(Material linea) {
		materiales.add(linea);
	}

	public void eliminarLineaDetalleMaterial(Material linea) {
		materiales.remove(linea);
	}
	
}
