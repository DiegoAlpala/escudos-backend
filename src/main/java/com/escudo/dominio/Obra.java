package com.escudo.dominio;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.checkerframework.common.aliasing.qual.Unique;

import com.escudo.dominio.base.EntidadBase;
import com.escudo.util.LocalDateDeserialize;
import com.escudo.util.LocalDateSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("serial")
@Entity
public class Obra extends EntidadBase {

	private int anios = 0;
	
	@Enumerated(EnumType.STRING)
	private Region region;
	
	@JsonSerialize(using = LocalDateSerialize.class)
	@JsonDeserialize(using = LocalDateDeserialize.class)
	private LocalDate fechaInicioGarantia;
	
	@Column(columnDefinition = "numeric(19,5)")
	private BigDecimal longitud;
	
	@Column(columnDefinition = "numeric(19,5)")
	private BigDecimal latitud;
	
	private String producto;
	
	private String lote;
	
	private BigDecimal dimension;
	
	private String nombrePropietario;
	
	private String razonSocial;
	
	@NotNull
	@Unique
	private String serie;
	
	private boolean generadoQR;
	
	private String ubicacionEscudo;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "socio_tecnico_id", referencedColumnName = "id", insertable = true, updatable = true)
	private SocioTecnico socio;
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true)
	@JoinColumn(name = "obra_id", nullable = false)
	private List<ImpermeabilizacionSistema> sistemas = new ArrayList<>();
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true)
	@JoinColumn(name = "obra_id", nullable = false)
	private List<EscudoGarantia> escudos = new ArrayList<>();
	
	@Enumerated(EnumType.STRING)
	private ObraEstado estado;

	@Column(columnDefinition = "varchar(max)")
	private String comentario;
	private String ciudad;

	@Transient
	private String nombreBroker;
	@Transient
	private String contactoBroker;

	public Obra() {	}

	public Obra(String serie ,SocioTecnico socio, List<EscudoGarantia> escudos) {
		this.serie = serie;
		this.socio = socio;
		this.escudos = escudos;
		estado= ObraEstado.PENDIENTE;				
	}

	public int getAnios() {
		return anios;
	}

	public void setAnios(int anios) {
		this.anios = anios;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public LocalDate getFechaInicioGarantia() {
		return fechaInicioGarantia;
	}

	public void setFechaInicioGarantia(LocalDate fechaInicioGarantia) {
		this.fechaInicioGarantia = fechaInicioGarantia;
	}

	public BigDecimal getLongitud() {
		return longitud;
	}

	public void setLongitud(BigDecimal longitud) {
		this.longitud = longitud;
	}

	public BigDecimal getLatitud() {
		return latitud;
	}

	public void setLatitud(BigDecimal latitud) {
		this.latitud = latitud;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public BigDecimal getDimension() {
		return dimension;
	}

	public void setDimension(BigDecimal dimension) {
		this.dimension = dimension;
	}

	public String getNombrePropietario() {
		return nombrePropietario;
	}

	public void setNombrePropietario(String nombrePropietario) {
		this.nombrePropietario = nombrePropietario;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public boolean isGeneradoQR() {
		return generadoQR;
	}

	public void setGeneradoQR(boolean generadoQR) {
		this.generadoQR = generadoQR;
	}

	public SocioTecnico getSocio() {
		return socio;
	}

	public void setSocio(SocioTecnico socio) {
		this.socio = socio;
	}

	public String getUbicacionEscudo() {
		return ubicacionEscudo;
	}

	public void setUbicacionEscudo(String ubicacionEscudo) {
		this.ubicacionEscudo = ubicacionEscudo;
	}

	public List<ImpermeabilizacionSistema> getSistemas() {
		return sistemas;
	}

	public void setSistemas(List<ImpermeabilizacionSistema> sistemas) {
		this.sistemas = sistemas;
	}

	public List<EscudoGarantia> getEscudos() {
		return escudos;
	}

	public void setEscudos(List<EscudoGarantia> escudos) {
		this.escudos = escudos;
	}
	
	public ObraEstado getEstado() {
		return estado;
	}

	public void setEstado(ObraEstado estado) {
		this.estado = estado;
	}

	public void agregarSistema(ImpermeabilizacionSistema sistema) {
		sistemas.add(sistema);
	}

	public void eliminarSistema(ImpermeabilizacionSistema sistema) {
		sistemas.remove(sistema);
	}
	
	public void agregarEscudo(EscudoGarantia escudo) {
		escudos.add(escudo);
	}

	public void eliminarEscudo(EscudoGarantia escudo) {
		escudos.remove(escudo);
	}

	public String getAsignado(){
		return getEstado().equals(ObraEstado.ASIGNADO)?"SI":"NO";
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getNombreBroker() {
		return nombreBroker;
	}

	public void setNombreBroker(String nombreBroker) {
		this.nombreBroker = nombreBroker;
	}

	public String getContactoBroker() {
		return contactoBroker;
	}

	public void setContactoBroker(String contactoBroker) {
		this.contactoBroker = contactoBroker;
	}
}
