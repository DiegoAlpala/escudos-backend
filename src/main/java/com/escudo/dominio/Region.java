package com.escudo.dominio;

public enum Region {

	COSTA,
	SIERRA,
	ORIENTE,
	INSULAR
}
