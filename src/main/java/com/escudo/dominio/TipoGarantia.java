package com.escudo.dominio;

public enum TipoGarantia {

	BRONCE,
	PLATINO,
	PLATA,
	ORO
}
