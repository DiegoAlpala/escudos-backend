package com.escudo.dominio;

import com.escudo.dominio.base.EntidadBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Data
@Entity
public class MaterialCatalogo extends EntidadBase {

    private String nombre;
    private boolean activo;

    public MaterialCatalogo(String nombre) {
        this.nombre = nombre;
        this.activo = true;
    }
}
