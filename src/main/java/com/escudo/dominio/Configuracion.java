package com.escudo.dominio;

import com.escudo.dominio.base.EntidadBase;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Configuracion extends EntidadBase {

    @Enumerated(EnumType.STRING)
    @NotNull
    private NombreConfiguracion nombreConfiguracion;
    private String valorConfiguracion;
    private String descripcion;
    private String expresionRegular;
}
