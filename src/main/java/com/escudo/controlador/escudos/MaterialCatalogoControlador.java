package com.escudo.controlador.escudos;

import com.escudo.dominio.MaterialCatalogo;
import com.escudo.servicio.IMaterialCatalogoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/materialesCatalogo")
public class MaterialCatalogoControlador {

	@Autowired
	private IMaterialCatalogoServicio servicio;

	@PostMapping()
	public ResponseEntity<MaterialCatalogo> registrarMaterial(@RequestBody MaterialCatalogo material) {
		MaterialCatalogo obj = servicio.registrar(material);
		return new ResponseEntity<MaterialCatalogo>(obj, HttpStatus.OK);
	}

	@PutMapping()
	public ResponseEntity<MaterialCatalogo> modificarMaterial(@RequestBody MaterialCatalogo material) {
		MaterialCatalogo obj = servicio.modificar(material);
		return new ResponseEntity<MaterialCatalogo>(obj, HttpStatus.OK);
	}

	@GetMapping()
	public ResponseEntity<List<MaterialCatalogo>> listarMateriales() {
		List<MaterialCatalogo> obj = servicio.listarTodos();
		return ResponseEntity.ok(obj);
	}

	@GetMapping("/activos")
	public ResponseEntity<List<MaterialCatalogo>> listarMaterialesActivos() {
		List<MaterialCatalogo> obj = servicio.listarActivos();
		return ResponseEntity.ok(obj);
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<Boolean> eliminarMaterial( @PathVariable("id")Long id) {
		Boolean obj = servicio.eliminar(id);
		return new ResponseEntity<Boolean>(obj, HttpStatus.OK);
	}
}
