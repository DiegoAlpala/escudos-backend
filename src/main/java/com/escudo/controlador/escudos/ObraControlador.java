package com.escudo.controlador.escudos;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.escudo.dominio.EscudoGarantia;
import com.escudo.dominio.ImpermeabilizacionSistema;
import com.escudo.dominio.Obra;
import com.escudo.dominio.ObraEstado;
import com.escudo.dominio.Region;
import com.escudo.dominio.TipoGarantia;
import com.escudo.dominio.dto.AsignarEscudoDTO;
import com.escudo.dominio.dto.CatalogoDTO;
import com.escudo.dominio.dto.ConsultaParametrosDTO;
import com.escudo.dominio.dto.ConsultaQR_DTO;
import com.escudo.dominio.dto.EscudosAsignadosResultadoDTO;
import com.escudo.dominio.dto.ObraConsultaDTO;
import com.escudo.servicio.IObraServicio;

@RestController
@RequestMapping("/obras")
public class ObraControlador {

	@Autowired
	private IObraServicio servicio;
	
	@GetMapping
	public ResponseEntity<List<Obra>> listarObras() {
		List<Obra> lista = servicio.listarTodos();
		return new ResponseEntity<List<Obra>>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Obra> listarPorId(@PathVariable("id") Long id) {
		Obra socio = servicio.listarPorId(id);
		return new ResponseEntity<Obra>(socio, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Obra> registrar(@Valid @RequestBody Obra obra) {
		Obra obj = servicio.registrar(obra);
		return new ResponseEntity<Obra>(obj, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<Obra> modificar(@Valid @RequestBody Obra obra) {
		Obra obj = servicio.modificar(obra);
		return new ResponseEntity<Obra>(obj, HttpStatus.OK);
	}
	
	@GetMapping("/serie/{serie}")
	public ResponseEntity<Obra> listarPorSerie(@PathVariable("serie") String serie) {
		Obra socio = servicio.listarPorSerie(serie);
		return new ResponseEntity<Obra>(socio, HttpStatus.OK);
	}
	
	@GetMapping("/regiones")
	public ResponseEntity<List<CatalogoDTO>> listarRegionesEnum() {
		Region[] options = Region.values();
		List<CatalogoDTO> catalog =new ArrayList<>();
		for(Region  ae : options ) {
			catalog.add(new CatalogoDTO(ae.toString(), ae.toString()));
		}
        return ResponseEntity.ok(catalog);
	}
	
	@GetMapping("/tipoGarantias")
	public ResponseEntity<List<CatalogoDTO>> listarTipoGarantiaEnum() {
		TipoGarantia[] options = TipoGarantia.values();
		List<CatalogoDTO> catalog =new ArrayList<>();
		for(TipoGarantia  ae : options ) {
			catalog.add(new CatalogoDTO(ae.toString(), ae.toString()));
		}
        return ResponseEntity.ok(catalog);
	}
	
	@GetMapping(value="/generarQR/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> cotizarDocumento(@PathVariable("id") Long id) {
		byte [] data = null;
		data= servicio.generarCodigoQR(id);
		return new ResponseEntity<byte[]>(data, HttpStatus.CREATED);
	}
	
	@PostMapping("/asignarGarantias")
	public ResponseEntity<List<EscudosAsignadosResultadoDTO>> asignarGarantias(@RequestBody AsignarEscudoDTO dto) {
		List<EscudosAsignadosResultadoDTO> obj = servicio.asignarEscudos(dto);
		return new ResponseEntity<List<EscudosAsignadosResultadoDTO>>(obj, HttpStatus.CREATED);
	}
	
	@PostMapping("/agregarSistema/{id}")
	public ResponseEntity<List<ImpermeabilizacionSistema>> agregarSistema(@PathVariable("id") Long id, @RequestBody ImpermeabilizacionSistema sistema) {
		List<ImpermeabilizacionSistema> obj = servicio.agregarSistema(id,sistema);
		return new ResponseEntity<List<ImpermeabilizacionSistema>>(obj, HttpStatus.CREATED);
	}
	
	@PostMapping("/agregarEscudo/{id}")
	public ResponseEntity<List<EscudoGarantia>> agregarEscudo(@PathVariable("id") Long id, @RequestBody EscudoGarantia escudo) {
		List<EscudoGarantia> obj = servicio.agregarEscudo(id,escudo);
		return new ResponseEntity<List<EscudoGarantia>>(obj, HttpStatus.CREATED);
	}
	
	@GetMapping("/consultaTodosQR")
	public ResponseEntity<List<ConsultaQR_DTO>> listarTodosQR() {
		List<ConsultaQR_DTO> lista = servicio.consultarQRTodos();
		return new ResponseEntity<List<ConsultaQR_DTO>>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/estado")
	public ResponseEntity<List<CatalogoDTO>> listarEstados() {
		ObraEstado[] options = ObraEstado.values();
		List<CatalogoDTO> catalog =new ArrayList<>();
		for(ObraEstado  ae : options ) {
			catalog.add(new CatalogoDTO(ae.toString(), ae.toString()));
		}
        return ResponseEntity.ok(catalog);
	}
	
	@PostMapping("/consulta")
	public ResponseEntity<Page<ObraConsultaDTO>> consultarObrasPaginado(final Pageable page,
			@RequestBody final ConsultaParametrosDTO consulta) {
		final Page<ObraConsultaDTO> resultadoConsulta = this.servicio.consultarObrasPaginado(page, consulta);
		return ResponseEntity.ok(resultadoConsulta);
	}

	@PostMapping(value="/consulta/todo", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> consultarObrasTotal(@RequestBody final ConsultaParametrosDTO consulta) {
		final byte[] resultadoConsulta = this.servicio.consultarObrasTotalExcel(consulta);
		return ResponseEntity.ok(resultadoConsulta);
	}

	@GetMapping(value="/reporteObraEscudoQr/{obraId}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteObraEscudoQr(@PathVariable("obraId") Long obraId) {
		final byte[] resultadoConsulta = this.servicio.generateReporteEscudo(obraId);
		return ResponseEntity.ok(resultadoConsulta);
	}

}
