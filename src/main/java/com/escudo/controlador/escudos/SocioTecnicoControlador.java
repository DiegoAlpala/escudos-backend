package com.escudo.controlador.escudos;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.escudo.dominio.SocioTecnico;
import com.escudo.servicio.ISocioTecnicoServicio;

@RestController
@RequestMapping("/socios")
public class SocioTecnicoControlador {

	@Autowired
	private ISocioTecnicoServicio servicio;
	
	@GetMapping
	public ResponseEntity<List<SocioTecnico>> listarSocios() {
		List<SocioTecnico> lista = servicio.listarTodos();
		return new ResponseEntity<List<SocioTecnico>>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<SocioTecnico> listarPorId(@PathVariable("id") Long id) {
		SocioTecnico socio = servicio.listarPorId(id);
		return new ResponseEntity<SocioTecnico>(socio, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<SocioTecnico> registrar(@Valid @RequestBody SocioTecnico socio) {
		SocioTecnico obj = servicio.registrar(socio);
		return new ResponseEntity<SocioTecnico>(obj, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<SocioTecnico> modificar(@Valid @RequestBody SocioTecnico socio) {
		SocioTecnico obj = servicio.modificar(socio);
		return new ResponseEntity<SocioTecnico>(obj, HttpStatus.OK);
	}

	@GetMapping("/activos")
	public ResponseEntity<List<SocioTecnico>> listarSociosActivos() {
		List<SocioTecnico> lista = servicio.listarActivos();
		return new ResponseEntity<List<SocioTecnico>>(lista, HttpStatus.OK);
	}
	
}
