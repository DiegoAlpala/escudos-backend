package com.escudo.controlador.escudos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.escudo.dominio.ImpermeabilizacionSistema;
import com.escudo.dominio.Material;
import com.escudo.servicio.IImpermeabilizacionSistemaServicio;

@RestController
@RequestMapping("/sistemas")
public class ImpermeabilizacionSistemaControlador {

	@Autowired
	private IImpermeabilizacionSistemaServicio servicio;
	
	@PostMapping("/agregarMaterial/{id}")
	public ResponseEntity<List<Material>> agregarMaterial(@PathVariable("id") Long id, @RequestBody Material material) {
		List<Material> obj = servicio.agregarMaterial(id, material);
		return new ResponseEntity<List<Material>>(obj, HttpStatus.CREATED);
	}
	
	@PutMapping()
	public ResponseEntity<ImpermeabilizacionSistema> modificarMaterial( @RequestBody ImpermeabilizacionSistema sistema) {
		ImpermeabilizacionSistema obj = servicio.modificar(sistema);
		return new ResponseEntity<ImpermeabilizacionSistema>(obj, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> eliminarSistema( @PathVariable("id")Long id) {
		Boolean obj = servicio.eliminar(id);
		return new ResponseEntity<Boolean>(obj, HttpStatus.OK);
	}
}
