package com.escudo.controlador.escudos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.escudo.dominio.Obra;
import com.escudo.servicio.IObraServicio;

@RestController
@RequestMapping("/publicaciones")
public class VistaPublicaControlador {

	@Autowired
	private IObraServicio servicio;
	
	
	@GetMapping("/informacionQR/{serie}")
	public ResponseEntity<Obra> listarPorSerie(@PathVariable("serie") String serie) {
		Obra socio = servicio.listarPorSerie(serie);
		return new ResponseEntity<Obra>(socio, HttpStatus.OK);
	}
}
