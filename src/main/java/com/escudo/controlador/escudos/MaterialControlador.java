package com.escudo.controlador.escudos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.escudo.dominio.Material;
import com.escudo.servicio.IMaterialServicio;

@RestController
@RequestMapping("/materiales")
public class MaterialControlador {

	@Autowired
	private IMaterialServicio servicio;
	
	@PutMapping()
	public ResponseEntity<Material> modificarMaterial( @RequestBody Material material) {
		Material obj = servicio.modificar(material);
		return new ResponseEntity<Material>(obj, HttpStatus.OK);
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<Boolean> eliminarMaterial( @PathVariable("id")Long id) {
		Boolean obj = servicio.eliminar(id);
		return new ResponseEntity<Boolean>(obj, HttpStatus.OK);
	}
}
