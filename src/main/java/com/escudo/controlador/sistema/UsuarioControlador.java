package com.escudo.controlador.sistema;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.escudo.dominio.dto.CatalogoDTO;
import com.escudo.dominio.sistema.Usuario;
import com.escudo.dominio.sistema.UsuarioTipo;
import com.escudo.servicio.sistema.IUsuarioServicio;

@RestController
@RequestMapping("/usuarios")
public class UsuarioControlador {

	@Autowired
	private IUsuarioServicio servicio;
	
	@GetMapping
	public ResponseEntity<List<Usuario>> listarUsuarios() {
		List<Usuario> lista = servicio.listarTodos();
		return new ResponseEntity<List<Usuario>>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Usuario> listarPorId(@PathVariable("id") Long id) {
		Usuario usuario = servicio.listarPorId(id);
		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Usuario> registrar(@Valid @RequestBody Usuario usuarioSistema) {
		Usuario obj = servicio.registrar(usuarioSistema);
		return new ResponseEntity<Usuario>(obj, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<Usuario> modificar(@Valid @RequestBody Usuario usuarioSistema) {
		Usuario obj = servicio.modificar(usuarioSistema);
		return new ResponseEntity<Usuario>(obj, HttpStatus.OK);
	}
	
	@GetMapping("/tipos")
	public ResponseEntity<List<CatalogoDTO>> listarTipoUsuarioEnum() {
		UsuarioTipo[] options = UsuarioTipo.values();
		List<CatalogoDTO> catalog =new ArrayList<>();
		for(UsuarioTipo  ae : options ) {
			catalog.add(new CatalogoDTO(ae.toString(), ae.toString()));
		}
        return ResponseEntity.ok(catalog);
	}
	
	@GetMapping("/cambiarContrasena/{id}/{contrasena}")
	public ResponseEntity<Boolean> listarPorId(@PathVariable("id") Long id, @PathVariable("contrasena") String contrasena) {
		servicio.cambiarContrasena(id, contrasena);
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

}
