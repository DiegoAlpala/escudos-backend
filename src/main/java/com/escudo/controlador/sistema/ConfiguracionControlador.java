package com.escudo.controlador.sistema;

import com.escudo.dominio.Configuracion;
import com.escudo.dominio.NombreConfiguracion;
import com.escudo.servicioImpl.ConfiguracionServicioImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
@Slf4j
@RestController
@RequestMapping("configuraciones")
public class ConfiguracionControlador {

    @Autowired
    private ConfiguracionServicioImpl servicio;

    @GetMapping("/enums")
    public ResponseEntity<List<NombreConfiguracion>> listarEnumConfiguraciones() {
        return new ResponseEntity<>(new ArrayList<NombreConfiguracion>(Arrays.asList(NombreConfiguracion.values())),
                HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Configuracion>> listarDescuentosPorCodigoBanner() {
        List<Configuracion> configuraciones = servicio.listarConfiguraciones();
        return ResponseEntity.ok(configuraciones);
    }

    @PutMapping
    public ResponseEntity<Configuracion> modificar(@RequestBody Configuracion dto) {
        Configuracion configuracion = servicio.modificar(dto);
        return ResponseEntity.ok(configuracion);
    }
}
