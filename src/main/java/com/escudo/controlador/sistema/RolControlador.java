package com.escudo.controlador.sistema;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.escudo.dominio.sistema.Menu;
import com.escudo.dominio.sistema.Rol;
import com.escudo.dominio.sistema.Roles;
import com.escudo.servicio.sistema.IRolServicio;

@RestController
@RequestMapping("/roles")
public class RolControlador {

	@Autowired
	private IRolServicio servicio;
	
	@GetMapping("/menuPorRol/{rol}")
	public ResponseEntity<List<Menu>> listarMenuPorRol(@PathVariable("rol") Roles rolNombre ) {
		List<Menu> lista = servicio.listarMenuPorRol(rolNombre);
		return new ResponseEntity<List<Menu>>(lista, HttpStatus.OK);
	}
	
	@GetMapping()
	public ResponseEntity<List<Rol>> listarTodos( ) {
		List<Rol> lista = servicio.listarTodos();
		return new ResponseEntity<List<Rol>>(lista, HttpStatus.OK);
	}
}
