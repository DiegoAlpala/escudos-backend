package com.escudo.repositorio;

import com.escudo.dominio.Material;
import com.escudo.repositorio.base.RepositorioBase;

public interface IMaterialRepositorio extends RepositorioBase<Material> {

}
