package com.escudo.repositorio;

import com.escudo.dominio.MaterialCatalogo;
import com.escudo.repositorio.base.RepositorioBase;

import java.util.List;
import java.util.Optional;

public interface IMaterialCatalogoRepositorio extends RepositorioBase<MaterialCatalogo> {

    List<MaterialCatalogo> findByActivoTrueOrderByNombreAsc();
    Optional<MaterialCatalogo> findByNombreIgnoreCase(String nombre);
}
