package com.escudo.repositorio;

import java.util.List;
import java.util.Optional;

import com.escudo.dominio.SocioTecnico;
import com.escudo.repositorio.base.RepositorioBase;


public interface ISocioTecnicoRepositorio extends RepositorioBase<SocioTecnico> {

	Optional<SocioTecnico> findByCodigoIdentificador(String codigoIdentificador);
	
	List<SocioTecnico> findByActivoTrue();
}
