package com.escudo.repositorio.sistema;

import com.escudo.dominio.sistema.SubMenu;
import com.escudo.repositorio.base.RepositorioBase;

public interface ISubMenuRepositorio extends RepositorioBase<SubMenu>{

}
