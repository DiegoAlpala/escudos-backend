package com.escudo.repositorio.sistema;

import com.escudo.dominio.sistema.Secuencial;
import com.escudo.repositorio.base.RepositorioBase;

public interface ISecuencialRepositorio extends RepositorioBase<Secuencial> {

}
