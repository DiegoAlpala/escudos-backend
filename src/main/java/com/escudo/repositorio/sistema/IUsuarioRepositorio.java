package com.escudo.repositorio.sistema;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.escudo.dominio.sistema.Usuario;
import com.escudo.dominio.sistema.UsuarioTipo;
import com.escudo.repositorio.base.RepositorioBase;

public interface IUsuarioRepositorio  extends RepositorioBase<Usuario>{

	Optional<Usuario> findByNombreUsuario(String nombre);
	
	Usuario findOneByNombreUsuario(String nombre);
	
	List<Usuario> findByTipoIn(Collection<UsuarioTipo> tipo);
}
