package com.escudo.repositorio.sistema;

import java.util.Optional;

import com.escudo.dominio.sistema.Rol;
import com.escudo.dominio.sistema.Roles;
import com.escudo.repositorio.base.RepositorioBase;

public interface IRolRepositorio extends RepositorioBase<Rol>{
	
	Optional<Rol> findByNombre(Roles nombre);

}
