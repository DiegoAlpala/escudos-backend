package com.escudo.repositorio.sistema;

import com.escudo.dominio.sistema.Menu;
import com.escudo.repositorio.base.RepositorioBase;


public interface IMenuRepositorio extends RepositorioBase<Menu>{

}
