package com.escudo.repositorio;

import java.util.Optional;

import com.escudo.dominio.Obra;
import com.escudo.repositorio.base.RepositorioBase;

public interface IObraRepositorio extends RepositorioBase<Obra> {

	Optional<Obra> findBySerie(String serie);
}
