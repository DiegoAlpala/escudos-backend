package com.escudo.repositorio;

import com.escudo.dominio.ImpermeabilizacionSistema;
import com.escudo.repositorio.base.RepositorioBase;

public interface IImpermeabilizacionSistemaRepositorio extends RepositorioBase<ImpermeabilizacionSistema> {

}
