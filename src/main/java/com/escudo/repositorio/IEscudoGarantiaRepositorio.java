package com.escudo.repositorio;

import com.escudo.dominio.EscudoGarantia;
import com.escudo.repositorio.base.RepositorioBase;

public interface IEscudoGarantiaRepositorio extends RepositorioBase<EscudoGarantia> {

}
