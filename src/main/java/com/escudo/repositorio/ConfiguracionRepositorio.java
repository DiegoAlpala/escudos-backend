package com.escudo.repositorio;

import com.escudo.dominio.Configuracion;
import com.escudo.dominio.NombreConfiguracion;
import com.escudo.repositorio.base.RepositorioBase;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface ConfiguracionRepositorio extends RepositorioBase<Configuracion> {
    Optional<Configuracion> findFirstByNombreConfiguracion(NombreConfiguracion nombreConfiguracion);
}
