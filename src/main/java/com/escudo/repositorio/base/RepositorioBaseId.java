package com.escudo.repositorio.base;

import org.springframework.data.repository.CrudRepository;

import com.escudo.dominio.base.EntidadBaseId;

public interface RepositorioBaseId<T extends EntidadBaseId> extends CrudRepository<T, Long>  {

}
