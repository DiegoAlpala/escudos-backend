package com.escudo.repositorio.base;

import com.escudo.dominio.base.EntidadBase;

public interface RepositorioBase<T extends EntidadBase> extends RepositorioBaseId<T> {

}
